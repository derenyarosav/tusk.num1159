package org.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> students = new ArrayList<>();
        students.add("Антонов");
        students.add("Шевченко");
        students.add("Бойко");
        students.add("Шевченко");
        students.add("Кравченко");
        students.add("Савицький");
        students.add("Антонов");
        students.add("Шевченко");
        students.add("Тетерів");
        students.add("Мамченко");
        for (String countOfstudents : students) {
            System.out.println(countOfstudents);
        }
        Set<String> studentSurname = new HashSet<String>();
        studentSurname.add("Антонов");
        studentSurname.add("Шевченко");
        studentSurname.add("Бойко");
        studentSurname.add("Шевченко");
        studentSurname.add("Кравченко");
        studentSurname.add("Кравченко");
        studentSurname.add("Савицький");
        studentSurname.add("Антонов");
        studentSurname.add("Шевченко");
        studentSurname.add("Тетерів");
        studentSurname.add("Мамченко");
        System.out.println(studentSurname.size());


        Map<Integer,String > studentsMap = new HashMap<Integer,String >();
        studentsMap.put(7,"Антонов" );
        studentsMap.put(8,"Шевченко");
        studentsMap.put(5,"Бойко");
        studentsMap.put(8,"Шевченко");
        studentsMap.put(9,"Кравченко");
        studentsMap.put(9,"Савицький");
        studentsMap.put(7,"Антонов");
        studentsMap.put(8,"Шевченко");
        studentsMap.put(7,"Тетерів");
        studentsMap.put(8,"Мамченко");
        System.out.println(studentsMap.get(9));
    }
}